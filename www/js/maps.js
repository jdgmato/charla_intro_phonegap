var app = {
  dispositivoListo: function(){
    navigator.geolocation.getCurrentPosition(app.pintaCoordenadas, app.errorAlSolicitarLocalizacion);
  },

  pintaCoordenadas: function (position) {
    var coordDiv = document.querySelector('#coords')
      coordDiv.innerHTML = 'Latitud: ' + position.coords.latitude + ' Longitud: ' + position.coords.longitude
  },

  errorAlSolicitarLocalizacion: function(error){
    console.log(error.code + ': ' + error.message);
  }

};

if ('addEventListener' in document) {
  document.addEventListener('deviceready', function() {
    app.dispositivoListo();
  }, false);
}
